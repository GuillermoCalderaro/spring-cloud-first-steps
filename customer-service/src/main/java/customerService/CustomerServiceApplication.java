package customerService;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import customerService.entity.Customer;
import customerService.entity.Region;
import customerService.repository.CustomerRepository;
import customerService.repository.RegionRepository;

@SpringBootApplication
@EnableEurekaClient
public class CustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner dataLoader(RegionRepository regionRepo, CustomerRepository customerRepository) {
		return new CommandLineRunner() {
			
			@Override
			public void run(String... args) throws Exception {
				regionRepo.save(new Region(Long.parseLong("1"), "Sudamérica"));
				regionRepo.save(new Region(Long.parseLong("2"), "Centroamérica"));
				regionRepo.save(new Region(Long.parseLong("3"), "Norteamérica"));
				regionRepo.save(new Region(Long.parseLong("4"), "Europa"));
				regionRepo.save(new Region(Long.parseLong("5"), "Asia"));
				regionRepo.save(new Region(Long.parseLong("6"), "Africa"));
				regionRepo.save(new Region(Long.parseLong("7"), "Oceanía"));
				regionRepo.save(new Region(Long.parseLong("8"), "Antártida"));
			
				customerRepository.save(Customer
										.builder()
										.id(Long.parseLong("1"))
										.numberID("32404580")
										.firstName("Andrés")
										.lastName("Guzmán")
										.email("profesor@bolsadeideas.com")
										.photoUrl("")
										.region(Region.builder().id(Long.parseLong("1")).build())
										.state("CREATED")
										.build());
				
		
			}
		};
	}

}
