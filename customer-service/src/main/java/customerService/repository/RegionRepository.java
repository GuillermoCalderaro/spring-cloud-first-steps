package customerService.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import customerService.entity.Region;

public interface RegionRepository extends JpaRepository<Region, Long>{

}
