package customerService.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import customerService.entity.Customer;
import customerService.entity.Region;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	Customer findByNumberID(String numberID);
	List<Customer> findByLastName(String lastName);
	List<Customer> findByRegion(Region region);
	
}
