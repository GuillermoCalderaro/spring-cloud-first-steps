package customerService.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import customerService.entity.Customer;
import customerService.entity.Region;
import customerService.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public List<Customer> findCustomerAll() {
		return (List<Customer>) customerRepository.findAll();	
	}

	@Override
	public List<Customer> findCustomerByRegion(Region region) {
		return customerRepository.findByRegion(region);
	}

	@Override
	public Customer createCustomer(Customer customer) {
		Customer customerDB = customerRepository.findByNumberID(customer.getNumberID());
		if (customerDB != null) {
			return customerDB;
		}
		customer.setState("CREATED");
		return customerRepository.save(customer);
	}

	@Override
	public Customer deleteCustomer(Customer customer) {
		Customer deletedCustomer = getCustomer(customer.getId());
		if (deletedCustomer == null) {
			return null;
		}
		deletedCustomer.setState("DELETED");
		return customerRepository.save(deletedCustomer);
	}

	@Override
	public Customer updateCustomer(Customer customer) {
		Customer customerDB = getCustomer(customer.getId());
        if (customerDB == null){
            return  null;
        }
        customerDB.setFirstName(customer.getFirstName());
        customerDB.setLastName(customer.getLastName());
        customerDB.setEmail(customer.getEmail());
        customerDB.setPhotoUrl(customer.getPhotoUrl());

        return  customerRepository.save(customerDB);
	}

	@Override
	public Customer getCustomer(Long id) {
		Customer customer = customerRepository.findById(id).orElse(null);
		return customer;
	}

}
