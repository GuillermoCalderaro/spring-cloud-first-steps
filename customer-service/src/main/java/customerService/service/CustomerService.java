package customerService.service;

import java.util.List;

import customerService.entity.Customer;
import customerService.entity.Region;

public interface CustomerService {

	List<Customer> findCustomerAll();
	List<Customer> findCustomerByRegion(Region region);
	Customer createCustomer(Customer customer);
	Customer deleteCustomer(Customer customer);
	Customer updateCustomer(Customer customer);
	Customer getCustomer(Long id);
	
}
