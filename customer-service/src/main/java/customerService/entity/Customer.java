package customerService.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "tbl_customers")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "The value of the customer id can not be empty")
	@Size(min = 8, max = 8, message = "The value of the customer id must contain 8 digits")
	@Column(name = "number_id", nullable = false, unique = true, length = 8)
	private String numberID;
	@NotEmpty(message = "The field of the customer first name is required")
	@Column(name = "first_name", nullable = false )
	private String firstName;
	@NotEmpty(message = "The field of the customer last name is required")
	@Column(name = "last_name", nullable = false )
	private String lastName;
	@NotEmpty(message = "The value of the customer mail can not be empty")
	@Email(message = "This is not a well formed email address")
	@Column(unique = true, nullable = false)
	private String email;
	@Column(name = "photo_url")
	private String photoUrl;
	@NotNull(message = "The region can not be empty")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Region region;
	private String state;
}

