package shoppingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import shoppingservice.entity.InvoiceItem;

public interface InvoiceItemRepository extends JpaRepository<InvoiceItem, Long> {

}
