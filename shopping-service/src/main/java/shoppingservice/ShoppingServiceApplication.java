package shoppingservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import shoppingservice.entity.Invoice;
import shoppingservice.entity.InvoiceItem;
import shoppingservice.repository.InvoiceItemRepository;
import shoppingservice.repository.InvoiceRepository;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
@EnableHystrixDashboard
public class ShoppingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner dataLoader(InvoiceRepository invoiceRepository, InvoiceItemRepository invoiceItemRepository) {
		return new CommandLineRunner() {
			
			@Override
			public void run(String... args) throws Exception {

				List<InvoiceItem> itemsList = new ArrayList<>();
				itemsList.add(InvoiceItem.builder()
						.id(Long.parseLong("1"))
						.productId(Long.parseLong("1"))
						.quantity(1.0)
						.price(178.89)
						.build());
				
				itemsList.add(InvoiceItem.builder()
						.id(Long.parseLong("2"))
						.productId(Long.parseLong("2"))
						.quantity(2.0)
						.price(12.5)
						.build());
				
				itemsList.add(InvoiceItem.builder()
						.id(Long.parseLong("3"))
						.productId(Long.parseLong("3"))
						.quantity(1.0)
						.price(40.06)
						.build());

				invoiceRepository.save(Invoice.builder()
													.id(Long.parseLong("1"))
													.numberInvoice("0001")
													.description("invoice office items")
													.customerId(Long.parseLong("1"))
													.createAt(new Date())
													.state("CREATED")
													.items(itemsList)
													.build());


				
			}
		};
	}
}
