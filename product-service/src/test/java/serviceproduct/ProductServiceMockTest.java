package serviceproduct;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.Data;
import serviceproduct.entity.Category;
import serviceproduct.entity.Product;
import serviceproduct.repository.ProductRepository;
import serviceproduct.service.ProductService;
import serviceproduct.service.ProductServiceImpl;

@SpringBootTest
@Data
public class ProductServiceMockTest {

	@Mock
	private ProductRepository productRepository;

	private ProductService productService;
	
	@SuppressWarnings("deprecation")
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		productService = new ProductServiceImpl(productRepository);
		
		Product computer = Product.builder()
				.id(1L)
				.name("computer")
				.description("")
				.category(Category.builder().id(1L).build())
				.price(Double.parseDouble("500"))
				.stock(Double.parseDouble("5"))
				.build();

		Mockito
			.when(productRepository.findById(1L))
			.thenReturn(Optional.of(computer));
		Mockito
			.when(productRepository.save(computer))
			.thenReturn(computer);
	}
	
	@Test
	public void whenValidGetId_ThenReturnProduct() {
		Product found = productService.getProductById(1L);
		Assertions.assertThat(found.getName()).isEqualTo("computer");
	}
	
	@Test
	public void whenValidUpdateStock_thenReturnNewStock() {
		Product updated = productService.updateStock(1L, 5.0);
		Assertions.assertThat(updated.getStock()).isEqualTo(10.0);
	}
}
