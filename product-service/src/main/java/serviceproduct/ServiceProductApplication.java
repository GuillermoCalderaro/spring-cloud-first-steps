package serviceproduct;

import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import serviceproduct.entity.Category;
import serviceproduct.entity.Product;
import serviceproduct.repository.CategoryRepository;
import serviceproduct.repository.ProductRepository;

@SpringBootApplication
@EnableEurekaClient
public class ServiceProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceProductApplication.class, args);
	}

	@Bean
	public CommandLineRunner dataLoader(CategoryRepository categoryRepository, ProductRepository productRepository) {
		return new CommandLineRunner() {
			
			@Override
			public void run(String... args) throws Exception {
				categoryRepository.save(new Category(Long.parseLong("1"), "shoes"));
				categoryRepository.save(new Category(Long.parseLong("2"), "books"));
				categoryRepository.save(new Category(Long.parseLong("3"), "electronics"));
			
				productRepository.save(Product.builder()
												.id(Long.parseLong("1"))
												.name("adidas Cloudfoam Ultimate")
												.description("Walk in the air in the black / black CLOUDFOAM ULTIMATE running shoe from ADIDAS")
												.stock(5.0)
												.price(178.89)
												.status("CREATED")
												.createAt(new Date())
												.category(Category.builder().id(Long.parseLong("1")).build())
												.build());

				
				productRepository.save(Product.builder()
						.id(Long.parseLong("2"))
						.name("adidas Cloudfoam Ultimate")
						.description("under armour Men ''Lightweight mesh upper delivers complete breathability . Durable leather overlays for stability")
						.stock(4.0)
						.price(12.5)
						.status("CREATED")
						.createAt(new Date())
						.category(Category.builder().id(Long.parseLong("1")).build())
						.build());

				productRepository.save(Product.builder()
						.id(Long.parseLong("3"))
						.name("Spring Boot in Action ")
						.description("under armour Men '' Craig Walls is a software developer at Pivotal and is the author of Spring in Action")
						.stock(12.0)
						.price(40.06)
						.status("CREATED")
						.createAt(new Date())
						.category(Category.builder().id(Long.parseLong("2")).build())
						.build());


			}
		};
	}
}
