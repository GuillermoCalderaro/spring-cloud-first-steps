package serviceproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import serviceproduct.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>{

}
