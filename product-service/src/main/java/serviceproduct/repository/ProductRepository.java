package serviceproduct.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import serviceproduct.entity.Category;
import serviceproduct.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

	public List<Product> findByCategory(Category category);
}
