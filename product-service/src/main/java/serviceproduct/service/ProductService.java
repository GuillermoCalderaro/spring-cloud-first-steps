package serviceproduct.service;

import java.util.List;

import serviceproduct.entity.Category;
import serviceproduct.entity.Product;

public interface ProductService {

	List<Product> listAllProduct();
	
	Product getProductById(Long id);
	
	Product createProduct(Product product);
	
	Product updateProduct(Product product);
	
	Product deleteProduct(Long id);
	
	List<Product> findByCategory(Category category);
	
	Product updateStock(Long id, Double quantity);

}
