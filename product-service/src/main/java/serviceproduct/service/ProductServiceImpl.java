package serviceproduct.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import serviceproduct.entity.Category;
import serviceproduct.entity.Product;
import serviceproduct.repository.ProductRepository;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{
	
	private final ProductRepository productRepository;
	
	@Override
	public List<Product> listAllProduct() {
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(Long id) {
		return productRepository.findById(id).orElse(null);
	}

	@Override
	public Product createProduct(Product product) {
		product.setStatus("CREATED");
		product.setCreateAt(new Date());
		return productRepository.save(product);		
	}

	@Override
	public Product updateProduct(Product product) {
		Product productBD = getProductById(product.getId());
		if (productBD == null) {
			return null;
		}
		productBD.setName(product.getName());
		productBD.setDescription(product.getDescription());
		productBD.setCategory(product.getCategory());
		productBD.setCreateAt(product.getCreateAt());
		productBD.setPrice(product.getPrice());
		productBD.setStock(product.getStock());
		productBD.setStatus(product.getStatus());
		
		return productRepository.save(productBD);
	}

	@Override
	public Product deleteProduct(Long id) {
		Product productBD = getProductById(id);
		if (productBD == null) {
			return null;
		}
		productBD.setStatus("DELETED");
		return productRepository.save(productBD);
		
	}

	@Override
	public List<Product> findByCategory(Category category) {
		return productRepository.findByCategory(category);
	}

	@Override
	public Product updateStock(Long id, Double quantity) {
		Product productBD = getProductById(id);
		if (productBD == null) {
			return null;
		}	
		productBD.setStock(quantity + productBD.getStock() );
		return productRepository.save(productBD);		
	}

}
